#!/usr/bin/env python3

import json
import re
import requests
from sys import argv


def filter_prod_cols(filters, prodData, mand_prod): # selects correct columns
    stdProdFlds = {} # standard product fields
    for field in filters["prodData"]["fields"].keys():
        stdProdFlds[field] = []

    for field, regx in filters["prodData"]["fields"].items():
        max_confid = 0 # stores max confidence yet
        match_found = False
        for key in prodData.keys():
            fldRegx = re.compile(regx, re.IGNORECASE)
            matches = 0 # to store matches found
            confid = 0 # confidence index
            poss_matches=0
            if fldRegx.search(key) and not match_found:
                stdProdFlds[field] = prodData[key]
                match_found = True
            for regxs in filters["prodData"]["cols"][field]:
                selector = re.compile(regxs, re.IGNORECASE)
                poss_matches = poss_matches+1
                if selector.search(prodData[key][0]):
                    matches = matches+1
                confid += matches/poss_matches * 100

            if mand_prod.count(field) > 0 and (not match_found) and confid > max_confid:
                max_confid = confid
                stdProdFlds[field] = prodData[key]

    filter_prod_vals(stdProdFlds, filters)

    return stdProdFlds


def filter_prod_vals(std_prod_flds, filters): # selects desired values
    for field in std_prod_flds:
        tmp = []
        for ele in std_prod_flds[field]:
            val = ""
            for reg in filters["prodData"]["values"][field]:
                regx = re.compile(reg, re.IGNORECASE)
                if regx.search(ele):
                    val = regx.search(ele).group(1)
                    break
            tmp.append(val)
        std_prod_flds[field] = tmp


def filter_details(filters, invInfo, mand_info):
    stdInvFlds = {}
    for field in filters["invData"]["fields"].keys():
        stdInvFlds[field] = ""

    for field, regx in filters["invData"]["fields"].items():
        keys = invInfo["Key"]
        vals = invInfo["Value"]
        fldRegx = re.compile(regx, re.IGNORECASE)
        match_found = False
        val = ""
        for i in range(len(keys)):
            valRegx = re.compile(filters["invData"]["values"][field])
            if fldRegx.search(keys[i]) and valRegx.match(vals[i]) and not match_found:
                val = valRegx.match(vals[i]).group(1)
                match_found = True
            elif mand_info.count(field)>0 and valRegx.match(vals[i]) and not match_found:
                val = valRegx.match(vals[i]).group(1)
        stdInvFlds[field] = val

    return stdInvFlds


if __name__ == "__main__":
    script, inp_file = argv
    r = requests.post('https://api.neopay.club/textract/upload-file/', files={ 'file' : open(inp_file, 'rb') })
    if not r.ok:
        print('request to api failed with status code : '+ str(r.status_code))
        exit()

    filters = eval(open("matching.json").read())
    data = json.loads(r.text)
    invInfo = data["data2"]
    prodData = data["data"]

    mand_info = [ # mandatory invoice details fields
        "seller gst",
        "buyer gst",
        "invoice number",
        "invoice data",
        "total amount"
    ]
    mand_prod = [ # mandatory product info fields
        "Product Name",
        "Quantity",
        "Unit",
        "MRP",
        "HSN Code",
        "Total Amount"
    ]

    std_prod_flds = filter_prod_cols(filters, prodData, mand_prod)
    std_inv_flds = filter_details(filters, invInfo, mand_info)

    print(json.dumps(std_prod_flds))
    print()
    print(json.dumps(std_inv_flds))
